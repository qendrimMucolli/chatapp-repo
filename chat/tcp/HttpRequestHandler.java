package challenge.chat.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class HttpRequestHandler implements Runnable {

    private final Logger log = Logger.getLogger(ChatServer.class.getCanonicalName());
    private ExecutorService threadPool = Executors.newCachedThreadPool();
    private final int connectionPort;
    private ChatServer chatServer;

    public HttpRequestHandler(int port, ChatServer chatServer) {
        connectionPort = port;
        this.chatServer = chatServer;
    }

    @Override
    public void run() {
        try (ServerSocket server = new ServerSocket(connectionPort)) {
            log.info("Accepting requests on port: " + connectionPort);
            Socket clientSocket;
            while (true) {
                clientSocket = server.accept();
                log.info("A new request has been accepted on the chat server!");
                Runnable client = new Request(chatServer, clientSocket);
                threadPool.execute(client);
            }
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
