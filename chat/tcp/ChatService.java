package challenge.chat.tcp;

import challenge.chat.entity.ChatRoom;
import challenge.chat.entity.User;

import java.util.Hashtable;
import java.util.Map;

public class ChatService {

    private Map<Integer, ChatRoom> rooms = new Hashtable<>();
    private Map<String, User> usersByToken = new Hashtable<>();

    public void addUserWithToken(String request) {
        String[] parameters = request.split(" ");
        User user = new User(parameters[1]);
        usersByToken.put(parameters[2], user);
    }

    public boolean isAuthenticated(String token) {
        return usersByToken.containsKey(token);
    }

    public void createRoom(int id, String name) {
        ChatRoom room = new ChatRoom(id, name);
        rooms.put(id, room);
    }

    public ChatRoom getJoinedRoom(int id, String token) {
        if (rooms.containsKey(id) && usersByToken.containsKey(token)) {
            ChatRoom room = rooms.get(id);
            User user = usersByToken.get(token);
            room.addUserToRoom(user);
            return room;
        }
        return null;
    }

    public void leaveRoom(int roomId, String userToken) {
        ChatRoom room = rooms.get(roomId);
        User user = usersByToken.get(userToken);
        room.removeUser(user);
    }

    public String getStringOfRooms() {
        return "List of available rooms: " + rooms.values();
    }

    public Map<Integer, ChatRoom> getChatRooms() {
        return rooms;
    }
}
