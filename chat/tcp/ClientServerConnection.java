
package challenge.chat.tcp;

import challenge.chat.entity.ChatRoom;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientServerConnection implements Runnable {

    private Socket clientSocket;
    private PrintWriter printWriter;
    private BufferedReader bufferedReader;
    private ChatRoom chatRoom;

    public ClientServerConnection(Socket clientSocket, ChatRoom room) {
        this.clientSocket = clientSocket;
        this.chatRoom = room;
        createWriterAndReader();
    }

    @Override
    public void run() {
        try {
            String name = bufferedReader.readLine();
            String chatServerMessage = chatRoom.getMessageHistory();
            chatRoom.sendAllMessagesToTheNewUser(chatServerMessage, this);
            chatServerMessage = name + " joined the chat room";
            sendMessageAndUpdateHistory(chatServerMessage);
            printRoomUsers();

            String userMessage = "";
            do {
                userMessage = bufferedReader.readLine();
                if (userMessage == null) break;
                chatServerMessage = "[" + name + "]: " + userMessage;
                sendMessageAndUpdateHistory(chatServerMessage);
            } while (true);
            clientSocket.close();
            chatServerMessage = name + " has left the chat room";
            sendMessageAndUpdateHistory(chatServerMessage);
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }

    public void printRoomUsers() {
        if (!chatRoom.hasUsersConnected()) {
            printWriter.println("Users in the chat room: " + chatRoom.getUserNames());
        } else {
            printWriter.println("There are no users in the chat room");
        }
    }

    public void sendMessageAndUpdateHistory(String chatServerMessage) {
        chatRoom.broadcastMessage(chatServerMessage, this);
        chatRoom.appendMessageToHistory(chatServerMessage + "\n");
    }

    public void sendMessage(String message) {
        printWriter.println(message);
    }

    private void createWriterAndReader() {
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            printWriter = new PrintWriter(clientSocket.getOutputStream(), true);
        } catch (IOException exc) {
            exc.printStackTrace();
        }

    }
}
