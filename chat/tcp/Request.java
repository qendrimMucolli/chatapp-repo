package challenge.chat.tcp;

import challenge.chat.entity.ChatRoom;

import java.io.*;
import java.net.Socket;

public class Request implements Runnable {

    private Socket clientSocket;
    private ChatServer chatServer;
    private BufferedReader bufferedReader;

    public Request(ChatServer chatServer, Socket clientSocket) {
        this.chatServer = chatServer;
        this.clientSocket = clientSocket;
        createReader();
    }

    @Override
    public void run() {
        ChatService chatService = chatServer.getChatService();
        try {
            String request = bufferedReader.readLine();

            if (request.matches("/register " + ".*\\d.*")) {
                chatService.addUserWithToken(request);
            } else if (request.matches("/createRoom " + ".*\\d.*" + " " + ".*\\s.*")) {
                String[] parameters = request.split(" ");
                int tokenPosition = parameters.length - 1;

                if (chatService.isAuthenticated(parameters[tokenPosition])) {
                    chatService.createRoom(Integer.parseInt(parameters[1]), parameters[tokenPosition - 1]);
                }
            } else if (request.matches("/joinRoom " + ".*\\d.*")) {
                ChatRoom room;
                String[] parameters = request.split(" ");
                int tokenPosition = parameters.length - 1;
                String token = parameters[tokenPosition];

                if (chatService.isAuthenticated(token)) {
                    try {
                        room = chatService.getJoinedRoom(Integer.parseInt(parameters[1]), token);
                        writeRoomToStream(room);
                    } catch (IOException exc) {
                        exc.printStackTrace();
                    }
                }
            } else if (request.matches("/leaveRoom " + ".*\\s.*")) {
                String[] parameters = request.split(" ");
                int roomId = Integer.parseInt(parameters[1]);
                String userToken = parameters[2];
                chatService.leaveRoom(roomId, userToken);
            } else if (request.matches("/listRooms " + ".*\\s.*")) {
                String response = chatService.getStringOfRooms();
                writeStringOfRoomsToStream(response);
            }

            clientSocket.close();
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }


    private void writeRoomToStream(ChatRoom room) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
        objectOutputStream.writeObject(room);
        objectOutputStream.flush();
    }

    private void writeStringOfRoomsToStream(String rooms) throws IOException {
        BufferedWriter clientWriter = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        clientWriter.write(rooms + "\n");
        clientWriter.flush();
    }

    private void createReader() {
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
