package challenge.chat.tcp;

public class ChatServer {

    private ChatService chatService = new ChatService();

    public void runChatServer() {
        int tcpPort = 3000;
        int httpPort = 8081;

        new Thread(new TcpRequestHandler(tcpPort, this)).start();
        new Thread(new HttpRequestHandler(httpPort, this)).start();
    }

    public ChatService getChatService() {
        return chatService;
    }

    public static void main(String[] args) {
        ChatServer chatServer = new ChatServer();
        chatServer.runChatServer();
    }
}
