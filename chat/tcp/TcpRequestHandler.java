package challenge.chat.tcp;

import challenge.chat.entity.ChatRoom;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class TcpRequestHandler implements Runnable {

    private final Logger log = Logger.getLogger(ChatServer.class.getCanonicalName());
    private ExecutorService threadPool = Executors.newCachedThreadPool();
    private final int connectionPort;
    private ChatServer chatServer;

    public TcpRequestHandler(int port, ChatServer chatServer) {
        connectionPort = port;
        this.chatServer = chatServer;
    }

    @Override
    public void run() {
        ChatService chatService = chatServer.getChatService();
        try (ServerSocket server = new ServerSocket(connectionPort)) {
            log.info("Accepting connections on port: " + connectionPort);
            Socket clientSocket;
            while (true) {
                clientSocket = server.accept();
                ChatRoom room;
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                int id = Integer.parseInt(bufferedReader.readLine());
                room = chatService.getChatRooms().get(id);
                log.info("A new connection was established with the chat server!");
                ClientServerConnection client = new ClientServerConnection(clientSocket, room);
                room.addConnection(client);
                threadPool.execute(client);
            }
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
