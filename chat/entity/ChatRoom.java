package challenge.chat.entity;

import challenge.chat.tcp.ClientServerConnection;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class ChatRoom implements Serializable {

    private int id;
    private String name;
    private Set<User> users;
    private transient List<ClientServerConnection> clientServerConnections;
    private String messageHistory = "";

    public ChatRoom(int id, String name) {
        this.id = id;
        this.name = name;
        users = new HashSet<>();
        clientServerConnections = new ArrayList<>();
    }

    public void addUserToRoom(User user) {
        users.add(user);
    }

    public void removeUser(User user) {
        users.remove(user);
    }

    public String getMessageHistory() {
        return messageHistory;
    }

    public void appendMessageToHistory(String messageHistory) {
        this.messageHistory += messageHistory;
    }

    public void broadcastMessage(String message, ClientServerConnection sender) {
        for (ClientServerConnection client : clientServerConnections) {
            if (client != sender) {
                client.sendMessage(message);
            }
        }
    }

    public int getId() {
        return id;
    }

    public void addConnection(ClientServerConnection clientServerConnection) {
        clientServerConnections.add(clientServerConnection);
    }

    public Set<String> getUserNames() {
        Set<String> temp = new HashSet<>();
        for (User user : users) {
            temp.add(user.getName());
        }
        return temp;
    }

    public void sendAllMessagesToTheNewUser(String message, ClientServerConnection newUser) {
        newUser.sendMessage(message);
    }

    public boolean hasUsersConnected() {
        return clientServerConnections.isEmpty();
    }

    @Override
    public String toString() {
        return " " + id + ": " + name + " ";
    }
}
