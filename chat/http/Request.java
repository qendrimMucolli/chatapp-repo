package challenge.chat.http;

import java.io.*;
import java.net.Socket;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Request implements Runnable {

    private final static Logger logger = Logger.getLogger(Request.class.getCanonicalName());
    private BufferedReader clientReader;
    private BufferedWriter clientWriter;
    private BufferedWriter tcpWriter;
    private Socket connection;

    public Request(Socket connection) {
        this.connection = connection;
        setupBuffers();
    }

    @Override
    public void run() {
        String request = "";
        try {
            int length = clientReader.read();
            for (int i = 0; i < length; i++) {
                request += clientReader.readLine() + " ";
            }
            if (!request.equals("")) {
                Socket tcpSocket = createChatServerHttpSocket();
                tcpWriter = new BufferedWriter(new OutputStreamWriter(tcpSocket.getOutputStream()));
                String[] messages = request.split(" ");
                String method = messages[0];
                String userRequest = rebuildRequest(messages);
                switch (method) {
                    case "POST":
                        if (userRequest.matches("/register " + ".*\\s.*")) {
                            authenticate(userRequest);
                        } else if (userRequest.matches("/createRoom " + ".*\\d.*" + ".*\\s.*")) {
                            sendRequest(userRequest);
                        }
                        break;
                    case "GET":
                        if (userRequest.matches("/joinRoom " + ".*\\d.*")) {
                            sendRequest(userRequest);
                            ObjectInputStream objectReader = new ObjectInputStream(tcpSocket.getInputStream());
                            ObjectOutputStream objectWriter = new ObjectOutputStream(connection.getOutputStream());
                            try {
                                objectWriter.writeObject(objectReader.readObject());
                                objectWriter.flush();
                            } catch (ClassNotFoundException exc) {
                                exc.printStackTrace();
                            }
                        } else if (userRequest.matches("/leaveRoom " + ".*\\d.*")) {
                            sendRequest(userRequest);
                        } else if (userRequest.matches("/listRooms " + ".*\\s.*")) {
                            sendRequest(userRequest);
                            BufferedReader tcpReader = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
                            String listOfRooms = tcpReader.readLine();
                            clientWriter.write(listOfRooms + "\n");
                            clientWriter.flush();
                        }
                }
                String infoMessage = userRequest.split(" ")[0];
                String info = connection.getRemoteSocketAddress() + " " + infoMessage;
                String contentType = "text";
                logHeader(info, contentType);
            }

        } catch (
                IOException ex) {
            logger.log(Level.WARNING,
                    "Error talking to " + connection.getRemoteSocketAddress(), ex);
        } finally {
            try {
                connection.close();
            } catch (IOException ex) {
            }
        }

    }

    private void logHeader(String info,
                           String contentType) {
        Date now = new Date();
        String httpMessage = info + "\r\n" +
                "HTTP/1.1 200 OK" + "\r\n" +
                "Date: " + now + "\r\n" +
                "Server: HTTPServer\r\n" +
                "Content-length: " + 2048 + "\r\n" +
                "Content-type: " + contentType + "\r\n";
        System.out.println();
        logger.info(httpMessage);
    }

    private void setupBuffers() {
        try {
            clientReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            clientWriter = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void sendRequest(String request) throws IOException {
        tcpWriter.write(request + "\n");
        tcpWriter.flush();
    }

    private void authenticate(String request) throws IOException {
        String[] parameters = request.split(" ");
        String token = generateToken(parameters[1]);
        tcpWriter.write(parameters[0] + " " + parameters[1] + " " + token + "\n");
        tcpWriter.flush();

        clientWriter.write(parameters[0] + " " + parameters[1] + " " + token + "\n");
        clientWriter.flush();
    }

    private String generateToken(String request) {
        String token = Integer.toString(request.hashCode());
        return token;
    }

    private String rebuildRequest(String[] messages) {
        String request = "";
        for (int i = 1; i < messages.length; i++) {
            if (messages[i].equals("Host:")) break;
            request += messages[i] + " ";
        }
        return request;
    }

    private Socket createChatServerHttpSocket() throws IOException {
        return new Socket("localhost", 8081);
    }
}

