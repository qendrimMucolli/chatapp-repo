package challenge.chat.client;

public class ChatClient {

    private void startListeningToRequests() {
        System.out.println();
        System.out.println("Use /register \"username\" to start the chat app");
        new Thread(new InputListener()).start();

    }

    public static void main(String[] args) {
        ChatClient chatClient = new ChatClient();
        chatClient.startListeningToRequests();
    }
}
