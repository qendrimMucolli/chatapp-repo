package challenge.chat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class ChatReader implements Runnable {

    private BufferedReader bufferedReader;
    private Socket clientSocket;

    ChatReader(Socket socket) {
        clientSocket = socket;
        createTheReader();
    }

    public void run() {
        while (true) {
            try {
                if (clientSocket.isClosed()) {
                    bufferedReader.close();
                    break;
                }
                String response = bufferedReader.readLine();
                if (response != null) {
                    System.out.println("\n" + response);
                }
            } catch (IOException exc) {
            }
        }
    }

    private void createTheReader() {
        try {
            InputStream input = clientSocket.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8));
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
