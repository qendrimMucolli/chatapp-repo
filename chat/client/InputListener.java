package challenge.chat.client;

import challenge.chat.entity.ChatRoom;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class InputListener implements Runnable {

    private Scanner scanner;
    private String token;
    private String userName;
    private Socket httpSocket;
    private Socket tcpSocket;
    private BufferedWriter writer;
    private boolean havingConversation;
    private int actualRoom;

    InputListener() {
        scanner = new Scanner(System.in);
    }

    @Override
    public void run() {
        String userInput;
        String httpFormat = "Host: localhost" + " " +
                "Connection: keep-alive " + " " +
                "Cache-Control: max-age=0" + " " +
                "Content-length: 2048" + " " +
                "Content-type: text" + " ";
        String httpMessage = "";

        try {
            do {
                userInput = scanner.nextLine();
                if (isRequest(userInput)) {
                    if (!havingConversation || userInput.equals("/leave")) {
                        if (userInput.matches("/joinRoom " + ".*\\d.*")) {
                            httpMessage = "GET " + userInput + " " + token + " " + httpFormat;
                            joinRoom(httpMessage);
                            havingConversation = true;
                        } else if (userInput.equals("/leave")) {
                            httpMessage = "GET " + "/leaveRoom " + actualRoom + " " + token + " " + httpFormat;
                            leaveRoom(httpMessage);
                            havingConversation = false;
                        } else if (userInput.equals("/listRooms all")) {
                            httpMessage = "GET " + userInput + " " + token + " " + httpFormat;
                            listRooms(httpMessage);
                        } else if (userInput.startsWith("/createRoom ")) {
                            httpMessage = "POST " + userInput + " " + token + " " + httpFormat;
                            createRoom(httpMessage);
                        } else if (userInput.startsWith("/register ")) {
                            httpMessage = "POST " + userInput + " " + token + " " + httpFormat;
                            register(httpMessage);
                            assignToken(httpSocket);
                            printStartingMessage();
                        }
                    } else {
                        System.out.println("You have to leave the room first");
                    }
                } else {
                    writeToChat(userInput);
                }
            } while (!userInput.equals("/logout"));
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }

    private void setToken(String token) {
        this.token = token;
    }

    private void setUserName(String name) {
        userName = name;
    }

    private void sendRoomIdToTcp(Socket httpSocket, String request) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpSocket.getOutputStream()));
        writer.write(request + " " + token + "\n");
        writer.flush();
    }

    private void startListeningToConversation(Socket tcpSocket, ExecutorService pool) {
        ChatReader chatReader = new ChatReader(tcpSocket);
        pool.execute(chatReader);
        pool.shutdown();
    }

    private boolean isRequest(String request) {
        return request.startsWith("/");
    }

    private void writeToChat(String message) {
        if (!tcpSocket.isClosed()) {
            try {
                writer.write(message + "\n");
                writer.flush();
            } catch (IOException exc) {
                exc.printStackTrace();
            }
        }
    }

    private void sendUsernameToChat(String userName) {
        writeToChat(userName);
        System.out.println("You have joined the chat room, messaging is now possible");
    }

    private void register(String httpMessage) throws IOException {
        createHttpSocket();
        writeToSocket(httpSocket, httpMessage);
    }

    private void joinRoom(String httpMessage) throws IOException {
        ExecutorService pool = Executors.newCachedThreadPool();
        createHttpSocket();
        createTcpSocket();
        writeToSocket(httpSocket, httpMessage);
        writer = new BufferedWriter(new OutputStreamWriter(tcpSocket.getOutputStream()));
        ObjectInputStream objectReader = new ObjectInputStream(httpSocket.getInputStream());
        try {
            ChatRoom room = (ChatRoom) objectReader.readObject();
            actualRoom = room.getId();
            sendRoomIdToTcp(tcpSocket, actualRoom + "\n");
            startListeningToConversation(tcpSocket, pool);
        } catch (ClassNotFoundException ecx) {
            System.out.println(ecx.getMessage());
        }
        sendUsernameToChat(userName);
    }

    private void leaveRoom(String request) throws IOException {
        createHttpSocket();
        writeToSocket(httpSocket, request);
        writer.close();
        System.out.println("You have left the chat room.");
    }

    private void listRooms(String httpMessage) throws IOException {
        createHttpSocket();
        writeToSocket(httpSocket, httpMessage);
        BufferedReader httpReader = new BufferedReader(new InputStreamReader(httpSocket.getInputStream()));
        String listOfRooms = httpReader.readLine();
        System.out.println(listOfRooms);
    }

    private void createRoom(String httpMessage) throws IOException {
        System.out.println("Chat room has been created");
        createHttpSocket();
        writeToSocket(httpSocket, httpMessage);
    }

    private void assignToken(Socket httpSocket) throws IOException {
        if (httpSocket != null && token == null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpSocket.getInputStream()));
            String[] parameters = reader.readLine().split(" ");
            setUserName(parameters[1]);
            setToken(parameters[2]);
        }
    }

    private void writeToSocket(Socket httpSocket, String request) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpSocket.getOutputStream()));
        String[] requestSplit = request.split(" ");
        writer.write(requestSplit.length);
        writer.flush();
        for (int i = 0; i < requestSplit.length; i++) {
            writer.write(requestSplit[i] + "\n");
            writer.flush();
        }
    }

    private void printStartingMessage() {
        System.out.println();
        System.out.println("You have been registered successfully");
        System.out.println("------------------------------");
        System.out.println("List of usable commands:");
        System.out.println("/createRoom \"id\" \"name\"");
        System.out.println("/joinRoom \"id\"");
        System.out.println("/listRooms all");
        System.out.println("/logout");
        System.out.println("------------------------------");
    }

    private void createHttpSocket() throws IOException {
        httpSocket = new Socket("localhost", 8080);
    }

    private void createTcpSocket() throws IOException {
        tcpSocket = new Socket("localhost", 3000);
    }
}
